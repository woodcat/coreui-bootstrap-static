var movieData = (function($){

  var _storage = storage=Storages.localStorage;
  var _customMovies = {};
  var _currentPage;

  var _myPrivateMethod = function(){
    alert('I am private!');
  };

  var _getPopularMovies = function(page, callback){
    var items = [];
    $.getJSON( "https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&page="+page+"&api_key="+$._api_key, function( data ) {
      if(data && data.results){
        $.each( data.results, function( key, val ) {
            if( storage.isSet(val.id)){
              console.log("movie "+val.id+" found in storage");
              _storedVal=storage.get(val.id);
              $.each(_storedVal, function (k, v){
                if(v){
                  val[k]=v;
                }
              })
            }
            
            m = new Movie(val.title, val.vote_average, val.poster_path, val.overview, val.id);
            console.log(m);
            items.push(m);
        });
      }
      callback(items);   
    });
  }

  var getPopularMovies = function(page, callback){
    var _p = page || 1;
    var items = _getPopularMovies(_p, callback);
    this._currentPage = page;
  }


  var editMovie = function(movie){
    storage.set(movie.getId(),movie);
  }

  var getCurrentPage = function(){
    return this._currentPage;
  }


  return {
      getPopularMovies : getPopularMovies,
      editMovie : editMovie,
      getCurrentPage : getCurrentPage
  };

})(jQuery);