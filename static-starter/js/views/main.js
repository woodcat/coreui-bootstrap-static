$(document).ready(function($){
  //first table population
  _createTable(1);

  _initFormListener();

});

//pagination
$('ul.pagination li a').on('click',function(e){
  console.log("moving to page " +$(this).text());
  _createTable($(this).text());
  $('ul.pagination li').removeClass('active');
  $(this).parent('li').addClass('active');
});


_createTable = function(page){
  movieData.getPopularMovies(page, function(items){
    var tableBody = $('#popularMovies > tbody').empty();
    $.each( items, function( key, val ) {  
      ($("<tr></tr>")
        .append($("<td></td>")
          .append(
            val.getId()+
            '<br>'+
            '<img id=\"barcode-'+val.getId()+'\"/>'+
            '<br>'+
            '<div id=\"qrcode-'+val.getId()+'\"></div>'
            )
        )
        .append($("<td></td>")
          .append(val.getTitle())
        )
        .append($("<td></td>")
          .append(val.getOverview())
        )
        .append($("<td></td>")
          .append(val.getVote_average())
        )
        .append($("<td></td>")
          .append("<img src=\""+val.getPoster_path('92')+"\">")
        )
      ).appendTo(tableBody);

     $("#barcode-"+val.getId()).JsBarcode(val.getId(),{
          format: "CODE128",
          lineColor: "#555"
        }
     );

    $("#qrcode-"+val.getId()).qrcode({

        text: val.getId(),
        size: 150
      });
    });
  });
}

_initFormListener = function(){
  console.log("init form listener");
  $('#formFilm-submit').on('click', function(e) {
        e.preventDefault();  //prevent form from submitting
        var data = $("#formFilm :input").serializeArray();
        var json = {};
    
        jQuery.each(data, function() {
            json[(this.name).substring((this.name).indexOf('-') + 1)  ] = this.value || '';
        });

        var _movie = new Movie(json.title, json.vote_average, '', json.overview, json.id);

        movieData.editMovie(_movie);

         _createTable(movieData.getCurrentPage);
    });
  
}
