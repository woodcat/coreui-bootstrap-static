function Movie(title, vote_average, poster_path, overview, id){
   this.title = title || '';
   this. vote_average = vote_average || '';
   this.poster_path = poster_path || '';
   this.overview = overview || '';
   this.id = id || '';
}

Movie.prototype.getTitle= function(){
   return this.title;
}

Movie.prototype.getVote_average= function(){
   return this.vote_average;
}

Movie.prototype.getPoster_path= function(width){
   /*
   "backdrop_sizes": [
    "w300",
    "w780",
    "w1280",
    "original"
    ],
    "logo_sizes": [
    "w45",
    "w92",
    "w154",
    "w185",
    "w300",
    "w500",
    "original"
    ],
    "poster_sizes": [
    "w92",
    "w154",
    "w185",
    "w342",
    "w500",
    "w780",
    "original"
    ],
    "profile_sizes": [
    "w45",
    "w185",
    "h632",
    "original"
    ],
    "still_sizes": [
    "w92",
    "w185",
    "w300",
    "original"
    ]
   */
   
    var w='w';
    switch(width){
        case '92':
           w=w+92;
           break;
        case '154':
           w=w+154;
           break;
        case '185':
           w=w+185;
           break;    
        case '342':
           w=w+342;
           break;
        case '500':
           w=w+500;
           break;
        case '780':
           w=w+780;
           break;
        default:
            w='original'            
   }
   
   return 'http://image.tmdb.org/t/p/'+w+'/'+this.poster_path;
}

Movie.prototype.getOverview= function(){
   return this.overview;
}

Movie.prototype.setId= function(id){
   this.id = id;
}

Movie.prototype.getId= function(){
   return this.id;
}